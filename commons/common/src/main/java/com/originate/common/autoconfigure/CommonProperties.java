package com.originate.common.autoconfigure;

import com.originate.common.su.CommonDef;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.io.Serializable;
import java.util.List;

/**
 * @author
 * @version 1.0
 * @date 2019/4/2 17:17
 * @describe
 */
@Data
@ConfigurationProperties(CommonDef.PROPERTIES_CONFIG_PREFIX)
public class CommonProperties implements Serializable {
	
	private String visits;
}
