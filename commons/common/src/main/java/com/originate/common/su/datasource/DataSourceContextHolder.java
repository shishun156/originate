package com.originate.common.su.datasource;

import lombok.extern.slf4j.Slf4j;


/**
 * @author
 * @version 1.0
 * @date 2019/5/19 16:26
 * @describe
 */
@Slf4j
public class DataSourceContextHolder {

    private static final ThreadLocal<DataSourceType> contextHolder = new ThreadLocal<>();

    public static void set(DataSourceType sourceType) {
        contextHolder.set(sourceType);
    }

    public static DataSourceType get() {
        return contextHolder.get();
    }

    public static void remove(){
        contextHolder.remove();
    }

    public static void change(DataSourceType sourceType){
        set(sourceType);
        log.info("################ switch database to {} ################",sourceType);
    }
}
