package com.originate.common.su.exception;

import java.text.MessageFormat;

/**
 * @author
 * @version 1.0
 * @date 2018/8/31 15:45
 * @describe
 */
public class ServiceException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private String code;

    private String message;

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    private ServiceException() {
        super();
    }

    private ServiceException(String message) {
        super(message);
        this.message = message;
    }

    private ServiceException(String code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    private ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    private ServiceException(Throwable cause) {
        super(cause);
    }

    public static ServiceException warn(String message) {
        return new ServiceException(message);
    }

    public static ServiceException warn(String code, String message) {
        return new ServiceException(code, message);
    }

    public static ServiceException warning(ErrorCode errorCode, Object... params) {
        return new ServiceException(errorCode.getStatus(), MessageFormat.format(errorCode.getDescribe(), params));
    }

    public static ServiceException warning(ErrorCode errorCode) {
        return new ServiceException(errorCode.getStatus(), errorCode.getDescribe());
    }

}
