package com.originate.common.su.util;
/**
 * @author
 * @version 1.0
 * @date 2018/9/23 0:15
 * @describe
 */
public class StringUtil extends CommonUtil{

    /**
     * 不区分大小写字符串比较
     *
     * @param arg0
     * @param arg1
     * @return
     */
    public static final boolean notDistinguishCaseEquals(String arg0, String arg1) {
        if (CommonUtil.isAllEmpty(arg0, arg1)) {
            return true;
        }
        if (CommonUtil.isAnyEmpty(arg0, arg1)) {
            return false;
        }
        return arg0.trim().equalsIgnoreCase(arg1.trim());
    }

    public static final boolean isBlank(String value) {
        if (isEmpty(value)) {
            return true;
        }
        if (value.trim().length() == 0) {
            return true;
        }
        return false;
    }

    public static final boolean isNotBlank(String value) {
        return !isBlank(value);
    }

    public static final boolean isBlankOrNull(String value) {
        if (isBlank(value)) {
            return true;
        }
        if ("null".equals(value.trim())) {
            return true;
        }
        return false;
    }

    public static final boolean notBlankOrNull(String value) {
        return !isBlankOrNull(value);
    }

    /**
     * 首字母大写
     *
     * @param arg0
     * @return
     */
    public static String capitalize(String arg0) {
        char[] chars = arg0.toCharArray();
        if (chars[0] >= 'a' && chars[0] <= 'z') {
            chars[0] -= 'a' - 'A';
        }
        return new String(chars);
    }
}
