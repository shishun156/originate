package com.originate.common.su.cache;

import java.lang.annotation.*;

/**
 * @author
 * @version 1.0
 * @date 2019/6/10 16:57
 * @describe
 */
@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER, ElementType.METHOD})
public @interface Cacheable {

    String key() default "";

    long expire() default -1;//单位秒

    long nullExpire() default 3 * 60;//null值缓存,时间
}
