package com.originate.common.su.datasource;

import java.lang.annotation.*;

/**
 * @author
 * @version 1.0
 * @date 2019/6/1 17:03
 * @describe
 */
@Inherited
@Documented
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DynamicSource {
    DataSourceType value();
}
