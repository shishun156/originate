package com.originate.common.plugin.oss.qiniu;

import lombok.Data;

import java.io.Serializable;

/**
 * @author shishun.wang
 * @version 1.0
 * @date 2019/7/10 22:33
 * @describe
 */
@Data
public class QiNiuConfig implements Serializable {
    private String reqPrefix;
    private String accessKey;
    private String secretKey;
    private String bucket;
}
