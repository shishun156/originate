package com.originate.common.su;

import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Getter
public enum DataStatus {

    AVAILABLE(1,"可用"),

    DISABLED(2,"禁用")
    ;
    DataStatus(int code,String display){
        this.code = code;
        this.display = display;
    }
    private int code;
    private String display;

    public static List<EnumVo> vos(){
        return Stream.of(DataStatus.values()).map(item -> EnumVo.builder().code(item.getCode()).display(item.getDisplay()).build()).collect(Collectors.toList());
    }

    public static DataStatus transform(int code){
        return Stream.of(DataStatus.values()).map(item -> {
            if (item.getCode() == code){
                return item;
            }
            return null;
        }).findFirst().get();
    }
}
