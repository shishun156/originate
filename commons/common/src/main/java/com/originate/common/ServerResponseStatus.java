package com.originate.common;

/**
 * @author
 * @version 1.0
 * @date 2019/5/22 0:32
 * @describe
 */
public enum ServerResponseStatus {

    SUCCESS,

    FAILURE
}

