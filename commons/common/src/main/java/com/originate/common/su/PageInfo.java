package com.originate.common.su;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author
 * @version 1.0
 * @date 2018/9/13 11:48
 * @describe
 */
@Data
public class PageInfo <T> implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final PageInfo EMPTY = new PageInfo();

    private int totalPage;

    private long totalSize;

    private List<T> content;

    public PageInfo() {
        this(Collections.emptyList(), 0, 0);
    }

    public PageInfo(List<T> content, int totalPage, long totalSize) {
        this.content = (List<T>) (content == null ? new ArrayList() : content);
        this.totalPage = totalPage < 0 ? 0 : totalPage;
        this.totalSize = totalSize < 0 ? 0 : totalSize;
    }


}
