package com.originate.common.su.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author shishun.wang
 * @version 1.0
 * @date 2019/7/10 23:01
 * @describe
 */
public class FileUtil {

    private FileUtil(){}

    public static byte[] toBytes(File file) throws IOException {
        FileInputStream stream = null;
        try {
            stream = new FileInputStream(file);
            int size = stream.available();
            byte data[] = new byte[size];
            stream.read(data);
            return data;
        } finally {
            if (null != stream)
                stream.close();
        }
    }
}
