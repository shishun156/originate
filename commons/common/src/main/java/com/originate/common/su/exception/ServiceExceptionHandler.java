package com.originate.common.su.exception;

import com.originate.common.su.ControllerResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * @author
 * @version 1.0
 * @date 2019/4/3 14:27
 * @describe
 */
@Slf4j
@ResponseBody
@RestControllerAdvice
public class ServiceExceptionHandler {
	
	@ResponseStatus(HttpStatus.OK)
	@ExceptionHandler(value = ServiceException.class)
	public Object serviceExceptionHandler(HttpServletRequest request, ServiceException e) throws Exception {
		log.error(e.getMessage(), e);
		return ControllerResult.failed(e.getCode(), e.getMessage());
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(value = Exception.class)
	public Object exceptionHandler(HttpServletRequest request, Exception e){
		log.error(e.getMessage(), e);
		return ControllerResult.failed(ErrorCode.SYSTEM_ERROR.getStatus(),ErrorCode.SYSTEM_ERROR.getDescribe());
	}
}
