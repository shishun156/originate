package com.originate.common.su.cache;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author
 * @version 1.0
 * @date 2019/6/10 16:57
 * @describe
 */
@Data
@NoArgsConstructor
public class CacheableContent<T> implements Serializable {

    private static final long max_expire = Long.MAX_VALUE;

    private T content;

    private long expireTime;

    public CacheableContent(T content, Cacheable cacheable) {
        this.content = content;
        if (this.content == null) {
            this.expireTime = (cacheable.nullExpire() * 1000) + System.currentTimeMillis();
            return;
        }

        if (cacheable.expire() == -1) {
            this.expireTime = Long.MAX_VALUE;
            return;
        }
        this.expireTime = System.currentTimeMillis() + (cacheable.expire() * 1000);
    }
}