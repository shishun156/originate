package com.originate.common.su.util;

/**
 * @author
 * @version 1.0
 * @date 2018/9/10 13:18
 * @describe
 */
public class BeanUtils<T> {

    public static <T> T copyProperties(Object source,T target, String... ignoreProperties){
        if (source == null){
            return null;
        }
        org.springframework.beans.BeanUtils.copyProperties(source,target,ignoreProperties);
        return target;
    }

    public static <T> T copyProperties(Object source,Class<T> objClass, String... ignoreProperties){
        if (source == null){
            return null;
        }
        T entity = org.springframework.beans.BeanUtils.instantiateClass(objClass);
        return copyProperties(source,entity,ignoreProperties);
    }
}
