package com.originate.common.autoconfigure;

import com.originate.common.su.CommonDef;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author
 * @version 1.0
 * @date 2019/4/2 17:16
 * @describe
 */
@Configuration
@EnableConfigurationProperties(CommonProperties.class)
@ConditionalOnProperty(prefix = CommonDef.PROPERTIES_CONFIG_PREFIX, value = "enable", matchIfMissing = true)
public class CommonAutoConfiguration {

	@Autowired
	private CommonProperties commonProperties;

}
