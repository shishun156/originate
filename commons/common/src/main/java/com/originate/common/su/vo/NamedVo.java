package com.originate.common.su.vo;

import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

/**
 * @author
 * @version 1.0
 * @date 2018/10/18 11:49
 * @describe
 */
@Getter
@Builder
public class NamedVo implements Serializable {
	
	private Long id;
	
	private String name;
}
