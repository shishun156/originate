package com.originate.common.su;

import com.originate.common.ServerResponseStatus;
import lombok.Data;

import java.io.Serializable;

/**
 * @author
 * @version 1.0
 * @date 2018/8/31 17:46
 * @describe
 */
@Data
public class ServerResponse<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private ServerResponseStatus status = ServerResponseStatus.SUCCESS;

    private String code;

    private String message;

    private T data;

    public ServerResponse<T> success() {
        return this;
    }

    public ServerResponse<T> success(T data) {
        this.data = data;
        return this;
    }

    public ServerResponse<T> failure() {
        this.status = ServerResponseStatus.FAILURE;
        return this;
    }

    public ServerResponse<T> failure(String message) {
        this.status = ServerResponseStatus.FAILURE;
        this.message = message;
        return this;
    }

    public ServerResponse<T> failure(String code, String message) {
        this.status = ServerResponseStatus.FAILURE;
        this.code = code;
        this.message = message;
        return this;
    }
}
