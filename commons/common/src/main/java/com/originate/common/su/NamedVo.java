package com.originate.common.su;

import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

@Getter
@Builder
public class NamedVo implements Serializable {
    private Long id;
    private String name;
}
