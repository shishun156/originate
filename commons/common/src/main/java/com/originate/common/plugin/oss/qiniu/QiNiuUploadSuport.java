package com.originate.common.plugin.oss.qiniu;

import com.google.gson.Gson;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
/**
 * @author shishun.wang
 * @version 1.0
 * @date 2019/7/10 22:35
 * @describe
 */
public class QiNiuUploadSuport {

    /**
     * @author shishun.wang
     * @version 1.0
     * @date 2018/10/5 1:21
     * @describe
     */
    public final static class Sample {

        private QiNiuConfig config;

        private String uploadUrl;

        private Sample(QiNiuConfig config){
            this.config = config;
        }

        public static Sample build(QiNiuConfig config){
            return new Sample(config);
        }

        public Sample upload(String fileName, byte[] uploadBytes) throws Exception {
            UploadManager uploadManager = new UploadManager(new Configuration());
            Auth auth = Auth.create(config.getAccessKey(), config.getSecretKey());
            String upToken = auth.uploadToken(config.getBucket());

            Response response = uploadManager.put(uploadBytes, fileName, upToken);
            DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
            this.uploadUrl = config.getReqPrefix()+"/"+putRet.key;
            return this;
        }

        public String builder(){
            return this.uploadUrl;
        }
    }
}
