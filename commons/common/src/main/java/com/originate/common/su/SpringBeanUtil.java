package com.originate.common.su;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author
 * @version 1.0
 * @date 2019/4/15 15:59
 * @describe
 */
@Component
public class SpringBeanUtil implements ApplicationContextAware {
	
	private static ApplicationContext applicationContext;
	
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		SpringBeanUtil.applicationContext = applicationContext;
	}
	
	public static <T> T getBean(Class<T> clazz) {
		return (T) applicationContext.getBean(clazz);
	}
	
	public static Object getBean(String name) throws BeansException {
		
		return applicationContext.getBean(name);
	}
}
