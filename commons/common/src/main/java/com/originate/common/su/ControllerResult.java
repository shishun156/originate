package com.originate.common.su;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * @author
 * @version 1.0
 * @date 2019/4/3 14:28
 * @describe
 */
public class ControllerResult {
	
	public static <Boolean> ResponseEntity<ServerResponse<Boolean>> success() {
		return new ResponseEntity<ServerResponse<Boolean>>(new ServerResponse<Boolean>().success(), HttpStatus.OK);
	}
	
	public static <Boolean> ResponseEntity<ServerResponse<Boolean>> failed() {
		return new ResponseEntity<ServerResponse<Boolean>>(new ServerResponse<Boolean>().failure(), HttpStatus.OK);
	}
	
	public static <T> ResponseEntity<ServerResponse<T>> success(T result) {
		return new ResponseEntity<ServerResponse<T>>(new ServerResponse<T>().success(result), HttpStatus.OK);
	}
	
	public static <T> ResponseEntity<ServerResponse<T>> failed(String message) {
		return new ResponseEntity<ServerResponse<T>>(new ServerResponse<T>().failure(message), HttpStatus.BAD_REQUEST);
	}
	
	public static <T> ResponseEntity<ServerResponse<T>> failed(Exception e) {
		return new ResponseEntity<ServerResponse<T>>(new ServerResponse<T>().failure(e.getMessage()),
				HttpStatus.BAD_REQUEST);
	}
	
	public static <T> ResponseEntity<ServerResponse<T>> failed(String code, String describe) {
		return new ResponseEntity<ServerResponse<T>>(new ServerResponse<T>().failure(code, describe),
				HttpStatus.BAD_REQUEST);
	}
}
