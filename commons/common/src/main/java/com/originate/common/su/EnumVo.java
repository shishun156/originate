package com.originate.common.su;

import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

@Getter
@Builder
public class EnumVo implements Serializable {

    private int code;
    private String display;

}
