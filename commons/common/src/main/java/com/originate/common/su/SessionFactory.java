package com.originate.common.su;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author
 * @version 1.0
 * @date 2019/4/3 14:33
 * @describe
 */
@Slf4j
public class SessionFactory {
	
	public final static HttpServletResponse getResponse() {
		HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
		return response;
	}
	
	public final static HttpServletRequest getRequest() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		return request;
	}
	
	/*public final static SubscriberJwt getCurrentUser(){
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		Object obj = request.getAttribute(CommonDef.SessionDef.TOKEN_USER_INFO);
		if (obj == null){
			throw ServiceException.warning(ErrorCode.SESSION_TIME_OUT);
		}
		return (SubscriberJwt)obj;
	}
	
	public final static Long getCurrentUserId() {
		return getCurrentUser().getId();
	}

	public final static String  getCurrentUserName(){
		return getCurrentUser().getName();
	}*/
}
