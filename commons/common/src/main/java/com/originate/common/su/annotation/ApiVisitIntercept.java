package com.originate.common.su.annotation;

import java.lang.annotation.*;

/**
 * @author
 * @version 1.0
 * @date 2019/5/24 23:56
 * @describe
 */
@Inherited
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ApiVisitIntercept {

    boolean visitor() default false;

    long visitRate() default 3000;

}
