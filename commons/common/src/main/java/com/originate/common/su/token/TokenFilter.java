package com.originate.common.su.token;


import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author
 * @version 1.0
 * @date 2019/6/24 14:50
 * @describe
 */
public class TokenFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpServletResponse response = (HttpServletResponse)servletResponse;
        //request.setAttribute(CommonDef.SessionDef.TOKEN_USER_INFO,TokenUtil.SubscriberToken.analysisToken(request.getHeader(CommonDef.SessionDef.AUTHORIZATION)));

        filterChain.doFilter(request,response);
    }
}
