package com.originate.common.su.datasource;

/**
 * @author
 * @version 1.0
 * @date 2019/5/19 16:27
 * @describe
 */
public enum DataSourceType {

    MASTER, SLAVE
}
