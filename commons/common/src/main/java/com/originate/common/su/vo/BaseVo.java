package com.originate.common.su.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author
 * @version 1.0
 * @date 2019/4/3 14:38
 * @describe
 */
@Data
public class BaseVo implements Serializable {

	private Long id;

	private String createdBy = "0";

	private String updatedBy = "0";

	private Integer version;

	//@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat( pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date createdTime = new Date();

	//@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat( pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date updatedTime = new Date();
	
}
