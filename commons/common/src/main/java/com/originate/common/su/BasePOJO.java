package com.originate.common.su;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * @author
 * @version 1.0
 * @date 2019/6/21 10:33
 * @describe
 */
@Data
public class BasePOJO implements Serializable {

    @Id
    @GeneratedValue(generator = "JDBC")
    private Long id;

    private String createdBy;

    private String updatedBy;

    private Date createdTime;

    private Date updatedTime;

    private Integer version;

    private Integer status;
}
