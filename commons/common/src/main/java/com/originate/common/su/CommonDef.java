package com.originate.common.su;

/**
 * @author
 * @version 1.0
 * @date 2019/4/2 17:23
 * @describe
 */
public interface CommonDef {
	
	String PROPERTIES_CONFIG_PREFIX = "com.originate.common";
	
	String CHARSET = "UTF-8";
	
	String DEFAULT_PASSWORD = "000000";
	
	public interface SessionDef {

		String TOKEN_USER_INFO = "AUTHORIZATION_TOKEN_USER_INFO";

		String AUTHORIZATION = "Authorization";

		String AUTHORIZATION_X_TOKEN = "Authorization-X-Token";
	}
}
