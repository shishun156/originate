package com.originate.core.apis;

import com.originate.core.autoconfigure.SystemDef;
import com.originate.core.service.SubscriberService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shishun.wang
 * @version 1.0
 * @date 2019/6/24 16:51
 * @describe
 */
@Validated
@ResponseBody
@RestController
@Api(tags = "用户管理")
@RequestMapping(SystemDef.Api.RESTFUL + "/subscriber")
public class SubscriberApi {

    @Autowired
    private SubscriberService subscriberService;



}
