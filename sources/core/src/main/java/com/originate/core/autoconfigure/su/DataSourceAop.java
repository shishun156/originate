package com.originate.core.autoconfigure.su;

import com.originate.common.su.datasource.DataSourceContextHolder;
import com.originate.common.su.datasource.DataSourceType;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author
 * @version 1.0
 * @date 2019/6/26 11:02
 * @describe
 */
@Slf4j
@Aspect
@Order(1)
@Component
@Lazy(false)
public class DataSourceAop {

    @Pointcut("!@annotation(com.originate.common.su.datasource.Master) " +
            "&& (execution(* com.originate.core.service..*.select*(..)) " +
            "|| execution(* com.originate.core.service..*.load*(..)) " +
            "|| execution(* com.originate.core.service..*.find*(..)) " +
            "|| execution(* com.originate.core.service..*.query*(..)) " +
            "|| execution(* com.originate.core.service..*.get*(..)))")
    public void readPointcut() {

    }

    @Pointcut("@annotation(com.originate.common.su.datasource.Master) " +
            "|| execution(* com.originate.core.service..*.insert*(..)) " +
            "|| execution(* com.originate.core.service..*.save*(..)) " +
            "|| execution(* com.originate.core.service..*.add*(..)) " +
            "|| execution(* com.originate.core.service..*.update*(..)) " +
            "|| execution(* com.originate.core.service..*.edit*(..)) " +
            "|| execution(* com.originate.core.service..*.delete*(..)) " +
            "|| execution(* com.originate.core.service..*.remove*(..))")
    public void writePointcut() {

    }

    @Pointcut("@annotation(com.originate.common.su.datasource.Master) " +
            "|| execution(* com.originate.core.service..*.*(..))")
    public void closeDataSource(){

    }

    @Before("readPointcut()")
    public void read() {
        DataSourceContextHolder.set(DataSourceType.SLAVE);
    }

    @Before("writePointcut()")
    public void write() {
        DataSourceContextHolder.set(DataSourceType.MASTER);
    }

    @After("closeDataSource()")
    public void after() {
        DataSourceContextHolder.remove();
    }
}
