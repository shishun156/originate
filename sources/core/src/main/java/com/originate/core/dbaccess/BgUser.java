package com.originate.core.dbaccess;

import com.originate.common.su.BasePOJO;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class BgUser extends BasePOJO {
    private String name;

    private String password;

    private String account;

    public BgUser(String name,String account){
        this.name = name;
        this.account = account;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }
}