package com.originate.core.apis.vo;

import com.originate.common.su.vo.BaseVo;
import lombok.Data;

@Data
public class BgUserVo extends BaseVo {

    private String name;

    private String password;

    private String account;
}
