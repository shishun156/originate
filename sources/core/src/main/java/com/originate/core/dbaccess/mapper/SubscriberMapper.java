package com.originate.core.dbaccess.mapper;

import com.originate.core.autoconfigure.MyMapper;
import com.originate.core.dbaccess.Subscriber;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SubscriberMapper extends MyMapper<Subscriber> {
    Subscriber selectByPrimaryKey(Long id);
}