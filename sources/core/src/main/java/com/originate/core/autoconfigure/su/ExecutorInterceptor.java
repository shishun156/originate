package com.originate.core.autoconfigure.su;

import com.originate.common.su.BasePOJO;
import com.originate.common.su.DataStatus;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.*;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Properties;

/**
 * @author
 * @version 1.0
 * @date 2019/6/22 1:12
 * @describe
 */
@Slf4j
@Component
@Intercepts({
        @Signature(type = Executor.class, method = "update", args = {MappedStatement.class, Object.class})
})
public class ExecutorInterceptor implements Interceptor {

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        MappedStatement ms = (MappedStatement) invocation.getArgs()[0];
        SqlCommandType commandType = ms.getSqlCommandType();
        Object parameter = invocation.getArgs()[1];
        String methodName = invocation.getMethod().getName();
        log.info("NormalPlugin, methodName; {}, commandType: {}", methodName, commandType);

        if (parameter instanceof BasePOJO) {
            BasePOJO pojo = (BasePOJO) parameter;

            pojo.setVersion(pojo.getVersion() == null ? 1 : pojo.getVersion() + 1);
            if (pojo.getId() != null) {
                //pojo.setUpdatedBy(SessionFactory.getCurrentUserId() + ":" + SessionFactory.getCurrentUserName());
                pojo.setUpdatedTime(new Date());
            } else {
                //pojo.setCreatedBy(SessionFactory.getCurrentUserId() + ":" + SessionFactory.getCurrentUserName());
                pojo.setCreatedTime(new Date());
                pojo.setUpdatedTime(pojo.getCreatedTime());
                pojo.setUpdatedBy(pojo.getCreatedBy());
                pojo.setStatus(DataStatus.AVAILABLE.getCode());
            }
        }

        return invocation.proceed();
    }

    @Override
    public Object plugin(Object o) {
        return Plugin.wrap(o, this);
    }

    @Override
    public void setProperties(Properties properties) {

    }
}
