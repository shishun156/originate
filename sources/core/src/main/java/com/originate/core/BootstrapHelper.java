package com.originate.core;

import com.originate.core.apis.vo.SubscriberVo;
import com.originate.core.service.SubscriberService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author shishun.wang
 * @version 1.0
 * @date 2019/6/21 17:17
 * @describe
 */
@Slf4j
@Component
public class BootstrapHelper implements ApplicationRunner {

    @Autowired
    private SubscriberService subscriberService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("系统启动成功");
        SubscriberVo subscriber = subscriberService.getSubscriberById(1l);
        System.out.println(subscriber.toString());
        subscriber.setName("王"+ DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now()));
        subscriberService.updateSubscriber(subscriber);
        System.out.println(subscriberService.getSubscriberById(1l));

        SubscriberVo sub = new SubscriberVo();{
            sub.setName("中华人名");
        }
        subscriberService.saveSubscriber(sub);
    }
}
