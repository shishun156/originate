package com.originate.core.dbaccess.mapper;

import com.originate.core.autoconfigure.MyMapper;
import com.originate.core.dbaccess.BgRole;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BgRoleMapper extends MyMapper<BgRole> {
    BgRole selectByPrimaryKey(Long id);
}