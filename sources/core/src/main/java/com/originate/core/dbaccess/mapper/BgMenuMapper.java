package com.originate.core.dbaccess.mapper;

import com.originate.core.autoconfigure.MyMapper;
import com.originate.core.dbaccess.BgMenu;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BgMenuMapper extends MyMapper<BgMenu> {
    BgMenu selectByPrimaryKey(Long id);
}