package com.originate.core.dbaccess.mapper;

import com.originate.core.autoconfigure.MyMapper;
import com.originate.core.dbaccess.BgPermission;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BgPermissionMapper extends MyMapper<BgPermission> {
    BgPermission selectByPrimaryKey(Long id);
}