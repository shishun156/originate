package com.originate.core.dbaccess.mapper;

import com.originate.core.autoconfigure.MyMapper;
import com.originate.core.dbaccess.BgUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BgUserMapper extends MyMapper<BgUser> {
    BgUser selectByPrimaryKey(Long id);
}