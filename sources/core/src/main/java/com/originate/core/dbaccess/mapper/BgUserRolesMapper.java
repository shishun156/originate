package com.originate.core.dbaccess.mapper;

import com.originate.core.autoconfigure.MyMapper;
import com.originate.core.dbaccess.BgUserRoles;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BgUserRolesMapper extends MyMapper<BgUserRoles> {
    BgUserRoles selectByPrimaryKey(Long id);
}