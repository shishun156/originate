package com.originate.core.service;

import com.originate.common.su.cache.Cacheable;
import com.originate.common.su.exception.ErrorCode;
import com.originate.common.su.exception.ServiceException;
import com.originate.common.su.util.BeanUtils;
import com.originate.core.apis.vo.SubscriberVo;
import com.originate.core.autoconfigure.SystemDef;
import com.originate.core.dbaccess.Subscriber;
import com.originate.core.dbaccess.mapper.SubscriberMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * @author shishun.wang
 * @version 1.0
 * @date 2019/6/21 17:24
 * @describe
 */
@Service
@Transactional
public class SubscriberService {

    @Autowired
    private SubscriberMapper subscriberMapper;

    @Cacheable(expire = SystemDef.SYSTEM_CACHE_EXPIRE)
    public SubscriberVo getSubscriberById(Long id){
        return BeanUtils.copyProperties(Optional.ofNullable(subscriberMapper.selectByPrimaryKey(id))
                .orElseThrow(()-> ServiceException.warning(ErrorCode.DATA_NOT_FOUND)),SubscriberVo.class);
    }

    public void updateSubscriber(SubscriberVo subscriberVo){
        subscriberMapper.updateByPrimaryKey(BeanUtils.copyProperties(subscriberVo, Subscriber.class));
    }

    public void saveSubscriber(SubscriberVo subscriberVo){
        subscriberMapper.insert(BeanUtils.copyProperties(subscriberVo,Subscriber.class));
    }
}
