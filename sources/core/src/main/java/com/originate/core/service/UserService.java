package com.originate.core.service;

import com.originate.common.su.exception.ErrorCode;
import com.originate.common.su.exception.ServiceException;
import com.originate.common.su.util.BeanUtils;
import com.originate.core.apis.vo.BgUserVo;
import com.originate.core.dbaccess.BgUser;
import com.originate.core.dbaccess.mapper.BgUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserService {

    @Autowired
    private BgUserMapper bgUserMapper;

    public void createBgUser(BgUserVo bgUserVo) {
        bgUserMapper.insert(BeanUtils.copyProperties(bgUserVo, BgUser.class));
    }

    public BgUserVo getBgUserById(Long id) {
        return BeanUtils.copyProperties(Optional.ofNullable(bgUserMapper.selectByPrimaryKey(id))
                .orElseThrow(() -> ServiceException.warning(ErrorCode.DATA_NOT_FOUND)), BgUserVo.class);
    }

    public List<BgUserVo> findBgUsers(String name,String account){
        return null;
    }

    public void findUserPermissions(Long userId){

    }

    public void findUserMenus(Long userId){

    }
}
