package com.originate.core.autoconfigure;

import com.originate.common.plugin.oss.qiniu.QiNiuConfig;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(SystemDef.PROPERTIES_CONFIG_PREFIX)
public class OriginateCoreProperties {

    private QiNiuConfig qiNiuConfig = new QiNiuConfig();

}
