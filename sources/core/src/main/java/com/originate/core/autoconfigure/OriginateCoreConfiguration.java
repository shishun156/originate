package com.originate.core.autoconfigure;

import com.originate.common.autoconfigure.CommonProperties;
import com.originate.core.autoconfigure.su.ExecutorInterceptor;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/**
 * @author
 * @version 1.0
 * @date 2019/5/27 16:25
 * @describe
 */
@Configuration
@EnableConfigurationProperties({ OriginateCoreProperties.class })
public class OriginateCoreConfiguration {

    @Autowired
    private CommonProperties commonProperties;

    @Bean
    public String myInterceptor(SqlSessionFactory sqlSessionFactory) {
        ExecutorInterceptor executorInterceptor = new ExecutorInterceptor();
        Properties properties = new Properties();
        properties.setProperty("prop1","value1");
        executorInterceptor.setProperties(properties);
        sqlSessionFactory.getConfiguration().addInterceptor(executorInterceptor);
        return "interceptor";
    }

    /*@Bean
    FilterRegistrationBean tokenFilter() {
        FilterRegistrationBean filterReg = new FilterRegistrationBean(new TokenFilter());
        filterReg.setOrder(Integer.MAX_VALUE);
        filterReg.setDispatcherTypes(DispatcherType.REQUEST);
        filterReg.addUrlPatterns("/*");
        if (StringUtil.notBlankOrNull(commonProperties.getVisits())){
            filterReg.addInitParameter("exclusions",commonProperties.getVisits());
        }
        return filterReg;
    }*/
}
