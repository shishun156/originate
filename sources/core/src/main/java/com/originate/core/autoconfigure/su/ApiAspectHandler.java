package com.originate.core.autoconfigure.su;

import com.originate.common.su.CommonDef;
import com.originate.common.su.annotation.ApiVisitIntercept;
import com.originate.common.su.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

@Slf4j
@Aspect
@Component
public class ApiAspectHandler {

    @Around("execution(* com.originate.core.apis.**Api.*(..))")
    public Object process(ProceedingJoinPoint point) throws Throwable {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        Method method = ((MethodSignature) point.getSignature()).getMethod();
        ApiVisitIntercept intercept = method.getAnnotation(ApiVisitIntercept.class);
        /*if (intercept != null && intercept.visitor()){
            request.setAttribute(CommonDef.SessionDef.TOKEN_USER_INFO, new SubscriberJwt(1l,"common"));
            return point.proceed();
        }
        if (StringUtil.isBlankOrNull(request.getHeader(CommonDef.SessionDef.AUTHORIZATION))){
            request.setAttribute(CommonDef.SessionDef.TOKEN_USER_INFO, TokenUtil.SubscriberToken.analysisToken(request.getHeader(CommonDef.SessionDef.AUTHORIZATION_X_TOKEN)));
        }else{
            request.setAttribute(CommonDef.SessionDef.TOKEN_USER_INFO, TokenUtil.SubscriberToken.analysisToken(request.getHeader(CommonDef.SessionDef.AUTHORIZATION)));
        }*/

        return point.proceed();
    }
}
