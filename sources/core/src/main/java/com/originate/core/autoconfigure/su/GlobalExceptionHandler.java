package com.originate.core.autoconfigure.su;

import com.originate.common.su.ControllerResult;
import com.originate.common.su.exception.ErrorCode;
import com.originate.common.su.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@ResponseBody
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    public Object handler(HttpServletRequest request, Exception e) throws Exception {
        log.error(e.getMessage(), e);
        if (e.toString().contains(ServiceException.class.getName())) {
            ServiceException serviceException = (ServiceException) e;
            return ControllerResult.failed(serviceException.getCode(), serviceException.getMessage());
        }
        return ControllerResult.failed(ErrorCode.SYSTEM_ERROR.getDescribe());
    }
}
