package com.originate.core.dbaccess;

import com.originate.common.su.BasePOJO;

public class BgMenu extends BasePOJO {
    private String menuName;

    private String menuUri;

    private Long parentMenuId;

    private String menuNotes;

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName == null ? null : menuName.trim();
    }

    public String getMenuUri() {
        return menuUri;
    }

    public void setMenuUri(String menuUri) {
        this.menuUri = menuUri == null ? null : menuUri.trim();
    }

    public Long getParentMenuId() {
        return parentMenuId;
    }

    public void setParentMenuId(Long parentMenuId) {
        this.parentMenuId = parentMenuId;
    }

    public String getMenuNotes() {
        return menuNotes;
    }

    public void setMenuNotes(String menuNotes) {
        this.menuNotes = menuNotes == null ? null : menuNotes.trim();
    }
}