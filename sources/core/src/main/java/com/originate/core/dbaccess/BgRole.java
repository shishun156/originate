package com.originate.core.dbaccess;

import com.originate.common.su.BasePOJO;

public class BgRole extends BasePOJO {
    private String roleName;

    private String roleNotes;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName == null ? null : roleName.trim();
    }

    public String getRoleNotes() {
        return roleNotes;
    }

    public void setRoleNotes(String roleNotes) {
        this.roleNotes = roleNotes == null ? null : roleNotes.trim();
    }
}