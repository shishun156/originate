package com.originate.core.apis.vo;

import com.originate.common.su.vo.BaseVo;
import lombok.Data;

/**
 * @author shishun.wang
 * @version 1.0
 * @date 2019/6/24 16:51
 * @describe
 */
@Data
public class SubscriberVo extends BaseVo {

    private String name;

    private String mobilePhone;

    private String email;
}
