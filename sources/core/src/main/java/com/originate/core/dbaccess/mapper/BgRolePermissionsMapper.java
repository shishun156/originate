package com.originate.core.dbaccess.mapper;

import com.originate.core.autoconfigure.MyMapper;
import com.originate.core.dbaccess.BgRolePermissions;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BgRolePermissionsMapper extends MyMapper<BgRolePermissions> {
    BgRolePermissions selectByPrimaryKey(Long id);
}