package com.originate.accredit;

import org.apache.catalina.Context;
import org.apache.tomcat.util.scan.StandardJarScanner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.annotation.Bean;

/**
 * @author shishun.wang
 * @version 1.0
 * @date 2019/6/20 15:16
 * @describe
 */
@SpringBootApplication
public class Bootstrap {

    @Bean
    public TomcatServletWebServerFactory tomcatFactory() {
        return new TomcatServletWebServerFactory() {
            @Override
            protected void postProcessContext(Context context) {
                ((StandardJarScanner) context.getJarScanner()).setScanManifest(false);
            }};
    }

    public static void main(String[] args) {
        SpringApplication.run(Bootstrap.class,args);
    }
}
