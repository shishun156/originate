package com.originate.accredit.service.impl;

import com.google.common.collect.Lists;
import com.originate.accredit.dbaccess.ApplicationAuth;
import com.originate.accredit.dbaccess.mapper.ApplicationAuthMapper;
import com.originate.common.su.exception.ErrorCode;
import com.originate.common.su.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private ApplicationAuthMapper applicationAuthMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        ApplicationAuth applicationAuth = new ApplicationAuth();
        applicationAuth.setName(username);
        ApplicationAuth domain = Optional.ofNullable(applicationAuthMapper.selectOne(applicationAuth)).orElseThrow(() -> ServiceException.warning(ErrorCode.DATA_NOT_FOUND));
        List<GrantedAuthority> grantedAuthorities = Lists.newArrayList();

        {
            GrantedAuthority grantedAuthority = new SimpleGrantedAuthority("ROLE_test");
            grantedAuthorities.add(grantedAuthority);
        }

        return new User(domain.getName(), domain.getPassword(), grantedAuthorities);
    }
}
