package com.originate.accredit.dbaccess.mapper;

import com.originate.accredit.dbaccess.ApplicationAuth;
import com.originate.core.autoconfigure.MyMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ApplicationAuthMapper extends MyMapper<ApplicationAuth> {

}