package com.originate.accredit.autoconfigure;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author
 * @version 1.0
 * @date 2019/5/3 11:04
 * @describe
 */
@Configuration
@EnableSwagger2
@Profile({ "dev", "test" })
public class SwaggerConfig {

    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage(SystemDef.Swagger.API_BASE_PACKAGE))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(metaData());
    }
    private ApiInfo metaData() {
        return new ApiInfoBuilder()
                .title(SystemDef.Swagger.TITLE)
                .description(SystemDef.Swagger.DESCRIPTION)
                .version(SystemDef.Swagger.VERSION)
                .build();
    }
}
