package com.originate.accredit.autoconfigure.su;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class MainApp {

    public static void main(String[] args) {
        String aa = new BCryptPasswordEncoder().encode("123456");
        String bb = new BCryptPasswordEncoder().encode("123456")+"11";

        System.out.println(aa);
        System.out.println(bb);
        System.out.println(new BCryptPasswordEncoder().matches("123456",aa));
        System.out.println(new BCryptPasswordEncoder().matches("123456",bb));
    }
}
