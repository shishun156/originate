package com.originate.core.autoconfigure;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * @author
 * @version 1.0
 * @date 2019/5/19 11:13
 * @describe
 */
public interface MyMapper<T> extends Mapper<T>, MySqlMapper<T> {
}
