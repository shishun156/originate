package com.originate.accredit.dbaccess;

import com.originate.common.su.BasePOJO;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ApplicationAuth extends BasePOJO {
    private String name;

    private String password;
}