package com.originate.accredit;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * @author shishun.wang
 * @version 1.0
 * @date 2019/6/21 17:17
 * @describe
 */
@Slf4j
@Component
public class BootstrapHelper implements ApplicationRunner {

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("系统启动成功");
    }
}
