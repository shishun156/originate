package com.originate.accredit.autoconfigure;

/**
 * @author
 * @version 1.0
 * @date 2019/5/17 11:27
 * @describe
 */
public interface SystemDef {

    String CACHE_NAME = "originate";

    long SYSTEM_CACHE_EXPIRE = 10;//60 * 30;//30分钟

    long REDISSION_MAX_WATING_TIME = 10;//10秒

    long REDISSION_AUTO_RELEASE_LOCK = 30;//30秒

    String DEFAULT_HEAD_PORTRAIT = "http://tool-mall.8tool.club/tx.jpg";

    String PROPERTIES_CONFIG_PREFIX = "com.originate.core";

    String VERSION = "3.0";

    public interface Api {

        String RESTFUL = "restful/" + VERSION;
    }

    public interface Swagger{

        String API_BASE_PACKAGE = "com.originate.accredit.apis";

        String VERSION = "3.0";

        String TITLE = "KK小程序接口管理";

        String DESCRIPTION ="KK小程序对外接口api";

        String ENVIRONMENT[] = {"dev","test"};
    }
}
