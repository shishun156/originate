package com.originate.accredit.autoconfigure.su;

import com.originate.common.su.datasource.DataSourceContextHolder;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import org.springframework.lang.Nullable;

/**
 * @author
 * @version 1.0
 * @date 2019/5/19 16:26
 * @describe
 */
public class CustomTargetRoutingDataSource extends AbstractRoutingDataSource {

    @Nullable
    @Override
    protected Object determineCurrentLookupKey() {
        return DataSourceContextHolder.get();
    }
}
