package com.originate.accredit.autoconfigure.su;

import com.originate.common.su.datasource.DataSourceType;
import com.originate.accredit.autoconfigure.su.CustomTargetRoutingDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author
 * @version 1.0
 * @date 2019/5/19 16:29
 * @describe
 */
@Configuration
public class DataSourceConfig {

    @Bean
    @ConfigurationProperties("spring.datasource.master")
    public DataSource masterDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    @ConfigurationProperties("spring.datasource.slave")
    public DataSource slave1DataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    public DataSource customTargetRoutingDataSource(@Qualifier("masterDataSource") DataSource masterDataSource,
                                          @Qualifier("slave1DataSource") DataSource slave1DataSource) {
        Map<Object,Object> content = new HashMap<Object,Object>();
        content.put(DataSourceType.SLAVE, slave1DataSource);
        content.put(DataSourceType.MASTER,masterDataSource);

        CustomTargetRoutingDataSource customTargetRoutingDataSource = new CustomTargetRoutingDataSource();
        customTargetRoutingDataSource.setDefaultTargetDataSource(masterDataSource);
        customTargetRoutingDataSource.setTargetDataSources(content);
        return customTargetRoutingDataSource;
    }
}
