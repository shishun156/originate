package com.originate.accredit.autoconfigure;

import com.originate.common.plugin.oss.qiniu.QiNiuConfig;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(SystemDef.PROPERTIES_CONFIG_PREFIX)
public class OriginateAccreditProperties {

    private QiNiuConfig qiNiuConfig = new QiNiuConfig();

}
