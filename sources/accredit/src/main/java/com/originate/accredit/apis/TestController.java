package com.originate.accredit.apis;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@Slf4j
@RequestMapping("/api/v1")
@RestController
public class TestController {

    @GetMapping("/get/user/info")
    public String getUserInfo(){
        return UUID.randomUUID().toString();
    }
}
