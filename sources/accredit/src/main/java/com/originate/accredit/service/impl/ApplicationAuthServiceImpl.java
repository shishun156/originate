package com.originate.accredit.service.impl;

import com.originate.accredit.dbaccess.mapper.ApplicationAuthMapper;
import com.originate.accredit.service.ApplicationAuthService;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@Transactional
public class ApplicationAuthServiceImpl implements ApplicationAuthService {

    @Mapper
    private ApplicationAuthMapper applicationAuthMapper;
}
