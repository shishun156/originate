// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import '../static/css/reset.css'
import './assets/font/iconfont.css'
import Vuex from 'vuex'
import store from './store/store'
import Api from './api/index.js'

Vue.use(Vuex)
Vue.use(Element)

Vue.prototype.$api = Api;
Vue.config.productionTip = false

//跳转路由之前查看是否登录，token是否过期
router.beforeEach((to, from, next) => {
  let user = window.localStorage.getItem('Authorization-Login');
  let token = window.localStorage.getItem('Authorization-Token');
  if (user) {
    if (to.path == '/login') {
      next()
    } else {
      if (token) {
        next()
      } else {
        Vue.prototype.$message('您的登录已过期，请重新登录');
        //sessionStorage.removeItem('token')
        window.localStorage.removeItem('Authorization-Token')
        next('/login')
      }
    }
  } else {
    if (to.path == '/login') {
      next()
    } else {
      Vue.prototype.$message.warning('您的登录已过期，请重新登录!');
      next('/login')
    }
  }
});

new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
