import axios from 'axios'

function apiAxios(method, req, params, response) {
  var http = axios.create({
    baseURL: '/api',
    withCredentials: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization-X-Token': window.localStorage.getItem('Authorization-Token'),
    }
  });

  http({
    method: method,
    url: req.url,
    data: method === 'POST' || method === 'PUT' ? params : null
  }).then(function (res) {
    console.log(res)
    if(res.data.status == "FAILURE"){
      alert(res.data.message)
      return ;
    }
    response({
      code: res.data.code,
      data: res.data.data,
      message: res.data.message
    });
  }).catch(function (err) {
    response({
      code: err.response.data.code,
      message: err.response.data.message
    });

    if (err.response.data.code == 'SESSION_TIME_OUT') {
      alert('会话超时');
      window.location.href = "/"
      return;
    }
  })
}

export default {
  get: function (req, response) {
    return apiAxios('GET', req, null, response)
  },
  post: function (req, params, response) {
    return apiAxios('POST', req, params, response)
  },
  put: function (req, params, response) {
    return apiAxios('PUT', req, params, response)
  },
  delete: function (req, response) {
    return apiAxios('DELETE', req, null, response)
  }
}
